package de.ostylk.roninmarket;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.*;
import java.util.function.Consumer;

public class PlayerHelper implements Listener {

    private static final Map<UUID, List<Consumer<Player>>> callbackQueues = new HashMap<>();

    public static void executeASAP(UUID owner, Consumer<Player> callback) {
        Player p = Bukkit.getPlayer(owner);
        if (p != null) {
            callback.accept(p);
            return;
        }

        List<Consumer<Player>> queue = callbackQueues.get(owner);
        if (queue == null) {
            queue = new ArrayList<>();
        }

        queue.add(callback);
        callbackQueues.put(owner, queue);
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        List<Consumer<Player>> queue = callbackQueues.get(e.getPlayer().getUniqueId());
        if (queue != null) {
            queue.forEach(consumer -> consumer.accept(e.getPlayer()));
        }
    }
}
