package de.ostylk.roninmarket.market;

import de.ostylk.baseapi.modules.config.settings.SettingLoad;
import de.ostylk.baseapi.modules.economy.Account;
import de.ostylk.baseapi.modules.economy.TransactionResult;
import de.ostylk.baseapi.modules.window.Window;
import de.ostylk.baseapi.modules.window.node.NodeContainer;
import de.ostylk.baseapi.modules.window.player.PlayerContext;
import de.ostylk.roninmarket.PlayerHelper;
import de.ostylk.roninmarket.RoninMarket;
import de.ostylk.roninmarket.math.AABB;
import de.ronin.post.PostAPI;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import java.util.function.Consumer;

public abstract class Market {

    @SettingLoad("pacht.laenge")
    private static long LEND_INTERVAL = 7 * 24 * 60 * 60 * 1000; // 7 days
    @SettingLoad("pacht.kosten")
    public static long EXTENSION_COST = 5000;

    /* Market Inventory Items */
    @SettingLoad("pacht.verlaengern.itemname")
    private static String LEND_EXTEND_ITEM_NAME = Material.EMERALD_BLOCK.toString();
    @SettingLoad("pacht.verlaengern.itemdata")
    private static int LEND_EXTEND_ITEM_DATA = 0;

    @SettingLoad("pacht.beenden.itemname")
    private static String LEND_CANCEL_ITEM_NAME = Material.BARRIER.toString();
    @SettingLoad("pacht.beenden.itemdata")
    private static int LEND_CANCEL_ITEM_DATA = 0;

    @SettingLoad("pacht.übertragen.itemname")
    private static String LEND_TRANSFER_ITEM_NAME = Material.MINECART.toString();
    @SettingLoad("pacht.übertragen.itemdata")
    private static int LEND_TRANSFER_ITEM_DATA = 0;

    private final int id;

    private final AABB area;
    private final World world;

    private UUID owner;
    private String ownerName;
    private long expireDate;

    private Window<NodeContainer> configWindow;
    
    public Market(int id, AABB area, World world, UUID owner, String ownerName, long expireDate) {
        this.id = id;
        this.area = area;
        this.world = world;
        this.owner = owner;
        this.ownerName = ownerName;
        this.expireDate = expireDate;
        
        this.buildConfigWindow();
    }
    
    private void buildConfigWindow() {
        this.configWindow = RoninMarket.WINDOW.createNodeWindow(9, 1, true);
        this.configWindow.getContainer().nodeCreator().button(0, 0,
                RoninMarket.ITEM.builder(Material.getMaterial(LEND_EXTEND_ITEM_NAME), (byte) LEND_EXTEND_ITEM_DATA)
                .name("\u00a7aPachtvertrag verlängern")
                .finish())
                .registerListener(instance -> {
                    MarketManager marketManager = RoninMarket.getInstance().getMarketManager();
                    PlayerContext context = RoninMarket.WINDOW.getContext(instance.getPlayer());
                    context.openConfirmation("\u00a7aPachtvertrag verlängern", player -> {
                            marketManager.extendMarket(player, this);
                        }, "Abbrechen!", player -> {},
                        RoninMarket.ITEM.builder(Material.PAPER).name("\u00a7aKostet " + RoninMarket.CURRENCY.formatAmount(EXTENSION_COST)).finish()
                    );
                });
        
        this.configWindow.getContainer().nodeCreator().button(4, 0,
                RoninMarket.ITEM.builder(Material.getMaterial(LEND_TRANSFER_ITEM_NAME), (byte) LEND_TRANSFER_ITEM_DATA)
                .name("\u00a7ePachtvertrag übertragen")
                .finish())
                .registerListener(instance -> {
                    MarketManager marketManager = RoninMarket.getInstance().getMarketManager();
                    marketManager.getTransferManager().beginTransfer(this);
                    PlayerContext context = RoninMarket.WINDOW.getContext(instance.getPlayer());
                    Bukkit.getScheduler().runTaskLater(RoninMarket.getInstance(), context::closeAll, 1L);
                });
        
        this.configWindow.getContainer().nodeCreator().button(8, 0,
                RoninMarket.ITEM.builder(Material.getMaterial(LEND_CANCEL_ITEM_NAME), (byte) LEND_CANCEL_ITEM_DATA)
                .name("\u00a7cPachtvertrag auflösen")
                .finish())
                .registerListener(instance -> {
                    MarketManager marketManager = RoninMarket.getInstance().getMarketManager();
                    PlayerContext context = RoninMarket.WINDOW.getContext(instance.getPlayer());
                    context.openConfirmation("\u00a7aPachtvertrag auflösen", player -> {
                            marketManager.unlendMarket(this);
                            Bukkit.getScheduler().runTaskLater(RoninMarket.getInstance(), context::close, 3L);
                        }, "Abbrechen!", player -> {}, null
                    );
                });
    }

    public void showParticles(Player p) {
        int deltaX = this.area.getMax().getBlockX() - this.area.getMin().getBlockX();
        int deltaY = this.area.getMax().getBlockY() - this.area.getMin().getBlockY();
        int deltaZ = this.area.getMax().getBlockZ() - this.area.getMin().getBlockZ();
        Location currentLoc = new Location(this.world,
                this.area.getMin().getBlockX() + 0.5D,
                this.area.getMin().getBlockY() + 0.5D,
                this.area.getMin().getBlockZ() + 0.5D);
        // Spawn particle asynchronously to not block the main thread
        Bukkit.getScheduler().runTaskAsynchronously(RoninMarket.getPlugin(RoninMarket.class), () -> {
            for (int ix = 0; ix <= Math.abs(deltaX); ix++) {
                for (int iy = 0; iy <= Math.abs(deltaY); iy++) {
                    for (int iz = 0; iz <= Math.abs(deltaZ); iz++) {
                        p.spawnParticle(Particle.HEART, currentLoc, 0);
                        currentLoc.add(0, 0, 1);
                    }
                    currentLoc.add(0, 1, -(deltaZ + 1));
                }
                currentLoc.add(1, -(deltaY + 1), 0);
            }
        });
    }

    public void openConfigWindow(Player player) {
        if (!isOccupied()) {
            System.err.println("I think there's are a bug in your code. This shouldn't be called!");
            return;
        }
        RoninMarket.WINDOW.getContext(player).open(this.configWindow, InventoryType.CHEST, this.ownerName + "s Markt");
    }

    /* PACKAGE PRIVATE METHODS */
    void lend(Player p, Consumer<Market> successCallback) {
        this.extend(p, target -> {
            if (this.isOccupied()) {
                // Avoid race condition
                target.sendMessage("\u00a7cDieser Markt ist bereits verpachtet.");
                return;
            }

            this.owner = target.getUniqueId();
            this.ownerName = target.getName();
            successCallback.accept(this);
        });
    }

    void extend(Player p, Consumer<Player> successCallback) {
        long maximum = System.currentTimeMillis() + 2 * LEND_INTERVAL;
        if (expireDate + LEND_INTERVAL > maximum) {
            p.sendMessage("\u00a7cDu darfst maximal 14 Tage im vorraus pachten.");
            return;
        }

        Account account = RoninMarket.ECONOMY.getAccount(p.getUniqueId(), RoninMarket.CURRENCY);
        TransactionResult result = account.withdraw(EXTENSION_COST);
        switch (result) {
            case SUCCESS:
                if (expireDate == 0L) {
                    this.expireDate = System.currentTimeMillis() + LEND_INTERVAL;
                } else {
                    this.expireDate += LEND_INTERVAL;
                }
                if (successCallback != null) successCallback.accept(p);
                break;
            case NOT_ENOUGH_MONEY:
                p.sendMessage("\u00a7cDu hast nicht genug Geld.");
                break;
            case OUT_OF_SPACE: // This shouldn't happen :)
                break;
        }
    }

    void unlend() {
        PlayerHelper.executeASAP(this.owner, p -> {
            p.sendMessage("\u00a7cDein Markt " + this.getID() + " ist abgelaufen.");
        });
        RoninMarket.getInstance().getShopManager().getShops().stream()
                .filter(shop -> shop.getParent() == this)
                .forEach(shop -> {
                    for (ItemStack item : shop.getShopContents()) {
                        if (item != null) PostAPI.sendItems(this.owner, item);
                    }
                    shop.clearShopContents();
                    shop.resetState();
                    RoninMarket.getInstance().getShopManager().saveShop(shop);
                });
        this.owner = null;
        this.ownerName = null;
        this.expireDate = 0L;
    }

    public void setOwner(UUID owner) { this.owner = owner; }
    public void setOwnerName(String ownerName) { this.ownerName = ownerName; }

    public boolean isOccupied() { return this.owner != null; }
    public boolean hasExpired() { return System.currentTimeMillis() > this.expireDate; }

    public int getID() { return this.id; }
    public AABB getArea() { return this.area; }
    public World getWorld() { return this.world; }
    public UUID getOwner() { return this.owner; }
    public String getOwnerName() { return this.ownerName; }
    public long getExpireDate() { return this.expireDate; }

    private static final SimpleDateFormat format = new SimpleDateFormat("dd.MM.yy");

    public String getExpireDateFormatted() {
        return format.format(new Date(this.expireDate));
    }
}
