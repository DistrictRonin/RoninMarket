package de.ostylk.roninmarket.market;

import de.ostylk.baseapi.modules.config.Config;
import de.ostylk.roninmarket.RoninMarket;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.List;

public class MarketIDAllocator {

    private Config backend;

    private List<Integer> freeIDs;
    private int startCountID;

    public MarketIDAllocator(Plugin plugin) {
        this.backend = RoninMarket.CONFIG.loadConfig(plugin, "marketIDs");
        this.load();
    }

    private void load() {
        this.backend.addDefault("startCountID", 1);
        this.backend.addDefault("freeIDs", new ArrayList<Integer>());
        this.startCountID = (int) this.backend.get("startCountID");
        this.freeIDs = this.backend.getIntegerList("freeIDs");
        if (this.freeIDs == null) {
            this.freeIDs = new ArrayList<>();
        }
    }

    private void updateConfig() {
        this.backend.set("startCountID", this.startCountID);
        this.backend.set("freeIDs", this.freeIDs);
        this.backend.save();
    }

    public int allocateID() {
        int id;
        if (this.freeIDs.isEmpty()) {
            id = this.startCountID++;
            this.updateConfig();
        } else {
            id = this.freeIDs.get(0);
            this.freeIDs.remove(0);
            this.updateConfig();
        }

        return id;
    }

    public void deallocateID(int id) {
        if (id == (this.startCountID - 1)) {
            this.startCountID--;
            this.updateConfig();
        } else {
            this.freeIDs.add(id);
            this.updateConfig();
        }
    }
}
