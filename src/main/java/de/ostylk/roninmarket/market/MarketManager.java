package de.ostylk.roninmarket.market;

import com.sk89q.worldedit.IncompleteRegionException;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.bukkit.BukkitPlayer;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.regions.Region;
import com.sk89q.worldedit.session.SessionManager;
import de.ostylk.baseapi.modules.config.Config;
import de.ostylk.roninmarket.RoninMarket;
import de.ostylk.roninmarket.market.shop.Shop;
import de.ostylk.roninmarket.market.transfer.TransferManager;
import de.ostylk.roninmarket.math.AABB;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MarketManager {

    private final Config backend;

    private final RoninMarket plugin;
    private final MarketIDAllocator idAllocator;

    private final TransferManager transferManager;

    private Map<Integer, Market> markets;

    public MarketManager(RoninMarket plugin) {
        this.backend = RoninMarket.CONFIG.loadConfig(plugin, "markets");

        this.plugin = plugin;
        this.idAllocator = new MarketIDAllocator(plugin);
        this.transferManager = new TransferManager(this);
        this.markets = new HashMap<>();

        this.load();
    }

    private void load() {
        this.backend.getSectionEntries("markets").forEach(marketKey -> {
            Market market = (Market) this.backend.get("markets." + marketKey);
            this.markets.put(market.getID(), market);
        });
    }

    public void notifyPlayerOnline(Player p) {
        this.markets.values().forEach(market -> {
            if (market.isOccupied() && market.getOwner().equals(p.getUniqueId())) {
                market.setOwnerName(p.getName());
            }
        });
    }

    public void notifyPlayerOffline(Player p) {
        this.transferManager.notifyPlayerOffline(p);
    }

    public void transferMarket(Market market, Player newOwner) {
        market.setOwner(newOwner.getUniqueId());
        market.setOwnerName(newOwner.getName());
        RoninMarket.getInstance().getMarketChiefManager().update();

        this.backend.set("markets." + market.getID(), market);
        this.backend.save();
    }

    public void lendMarket(Player p, Market market) {
        if (market.isOccupied()) return;

        market.lend(p, callbackMarket -> {
            RoninMarket.getInstance().getMarketChiefManager().update();
            this.backend.set("markets." + callbackMarket.getID(), callbackMarket);
            this.backend.save();
        });
    }

    public void extendMarket(Player p, Market market) {
        market.extend(p, pt -> {
            RoninMarket.getInstance().getMarketChiefManager().update();
            this.backend.set("markets." + market.getID(), market);
            this.backend.save();
        });
    }

    public void unlendMarket(Market market) {
        market.unlend();
        RoninMarket.getInstance().getMarketChiefManager().update();
        this.backend.set("markets." + market.getID(), market);
        this.backend.save();
    }

    public Market createMarket(Player p) {
        int id = this.idAllocator.allocateID();
        BukkitPlayer player = new BukkitPlayer(WorldEditPlugin.getPlugin(WorldEditPlugin.class), p);
        SessionManager manager = WorldEdit.getInstance().getSessionManager();
        Region selection = null;
        try {
            selection = manager.get(player).getSelection(manager.get(player).getSelectionWorld());
        } catch (IncompleteRegionException e) {
            e.printStackTrace();
        }
        double minX = selection.getMinimumPoint().getX(), maxX = selection.getMaximumPoint().getX();
        double minY = selection.getMinimumPoint().getY(), maxY = selection.getMaximumPoint().getY();
        double minZ = selection.getMinimumPoint().getZ(), maxZ = selection.getMaximumPoint().getZ();
        AABB aabb = new AABB(new Vector(minX, minY, minZ), new Vector(maxX, maxY, maxZ));

        Market market = new MarketSerializer(id, aabb, Bukkit.getWorld(selection.getWorld().getName()), null, null, 0L);
        this.markets.put(id, market);
        RoninMarket.getInstance().getMarketChiefManager().update();
        this.backend.set("markets." + market.getID(), market);
        this.backend.save();
        return market;
    }

    public Market getMarket(int id) {
        return this.markets.get(id);
    }

    public Market getMarket(Location loc) {
        for (Market market : this.markets.values()) {
            if (market.getWorld().getName().equals(loc.getWorld().getName())) {
                if (market.getArea().isInside(loc.toVector())) {
                    return market;
                }
            }
        }
        return null;
    }

    public boolean deleteMarket(int id) {
        if (!this.markets.containsKey(id)) {
            // ID doesn't exist
            return false;
        }

        Market market = this.markets.remove(id);
        List<Shop> toDelete = new ArrayList<>();
        this.plugin.getShopManager().getShops().forEach(toDelete::add);
        toDelete.forEach(shop -> this.plugin.getShopManager().removeShop(shop));
        this.idAllocator.deallocateID(market.getID());
        RoninMarket.getInstance().getMarketChiefManager().getMarketChiefs().forEach((chiefID, chief) -> {
            chief.unlink(market);
        });
        this.plugin.getShopManager().getShops().forEach(shop -> {
            if (shop.getParent().getID() == id) {
                this.plugin.getShopManager().removeShop(shop);
            }
        });
        RoninMarket.getInstance().getMarketChiefManager().update();
        this.backend.set("markets." + id, null);
        this.backend.save();
        return true;
    }

    public TransferManager getTransferManager() { return this.transferManager; }
    public Map<Integer, Market> getMarkets() { return this.markets; }
}
