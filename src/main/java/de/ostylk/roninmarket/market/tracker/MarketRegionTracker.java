package de.ostylk.roninmarket.market.tracker;

import de.ostylk.roninmarket.market.Market;
import de.ostylk.roninmarket.market.MarketManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.HashMap;
import java.util.Map;

public class MarketRegionTracker implements Listener {

    private final MarketManager marketManager;

    private final Map<String, Market> playerInMarket;

    public MarketRegionTracker(MarketManager marketManager) {
        this.marketManager = marketManager;

        this.playerInMarket = new HashMap<>();
    }

    @EventHandler
    public void onMove(PlayerMoveEvent e) {
        if (e.getFrom().getBlockX() == e.getTo().getBlockX() &&
                e.getFrom().getBlockY() == e.getTo().getBlockY() &&
                e.getFrom().getBlockZ() == e.getTo().getBlockZ()) {
            // Don't check if the player is still on the same block
            return;
        }

        Player p = e.getPlayer();
        Market market = this.marketManager.getMarket(e.getTo());
        if (market != null) {
            // Player is in a market area
            // But first time entering it
            if (!this.playerInMarket.containsKey(p.getName())) {
                this.playerInMarket.put(p.getName(), market);
                Bukkit.getPluginManager().callEvent(new MarketRegionEnter(p, market));
            }
        } else {
            // Player is not on market area
            // But was before
            if (this.playerInMarket.containsKey(p.getName())) {
                market = this.playerInMarket.remove(p.getName());
                Bukkit.getPluginManager().callEvent(new MarketRegionLeave(p, market));
            }
        }
    }

}
