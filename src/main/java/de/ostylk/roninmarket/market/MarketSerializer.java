package de.ostylk.roninmarket.market;

import de.ostylk.roninmarket.math.AABB;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.configuration.serialization.ConfigurationSerializable;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class MarketSerializer extends Market implements ConfigurationSerializable {

    public MarketSerializer(int id, AABB area, World world, UUID owner, String ownerName, long expireDate) {
        super(id, area, world, owner, ownerName, expireDate);
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> data = new HashMap<>();

        data.put("id", this.getID());
        data.put("area", this.getArea().encode());
        data.put("world", this.getWorld().getName());

        if (this.isOccupied()) {
            Map<String, Object> ownerData = new HashMap<>();

            ownerData.put("uuid", this.getOwner().toString());
            ownerData.put("name", this.getOwnerName());
            ownerData.put("expireDate", this.getExpireDate());

            data.put("owner", ownerData);
        }

        return data;
    }

    public static MarketSerializer deserialize(Map<String, Object> data) {
        int id = (int) data.get("id");
        AABB area = AABB.decode((String) data.get("area"));
        World world = Bukkit.getWorld((String) data.get("world"));

        UUID owner = null;
        String ownerName = null;
        long expireDate = 0L;
        if (data.containsKey("owner")) {
            Map<String, Object> ownerData = (Map<String, Object>) data.get("owner");

            owner = UUID.fromString((String) ownerData.get("uuid"));
            ownerName = (String) ownerData.get("name");
            expireDate = (long) ownerData.get("expireDate");
        }

        return new MarketSerializer(id, area, world, owner, ownerName, expireDate);
    }
}
