package de.ostylk.roninmarket.market.shop;

import de.ostylk.baseapi.modules.config.settings.SettingLoad;
import de.ostylk.baseapi.modules.economy.Account;
import de.ostylk.baseapi.modules.window.Container;
import de.ostylk.baseapi.modules.window.Window;
import de.ostylk.baseapi.modules.window.WindowInstance;
import de.ostylk.baseapi.modules.window.node.NodeContainer;
import de.ostylk.baseapi.modules.window.node.elements.Button;
import de.ostylk.baseapi.modules.window.storage.FilterResult;
import de.ostylk.baseapi.modules.window.storage.StorageContainer;
import de.ostylk.baseapi.modules.window.storage.event.FilterEvent;
import de.ostylk.baseapi.modules.window.storage.event.FilterEventType;
import de.ostylk.roninmarket.RoninMarket;
import de.ostylk.roninmarket.market.Market;
import de.ostylk.roninmarket.util.InventoryHelper;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

public abstract class Shop {

    /* Shop Inventory Items */
    @SettingLoad("shop.aktion1.itemname")
    private static String ACTION_1_MATN = Material.IRON_BLOCK.toString();
    @SettingLoad("shop.aktion1.itemdata")
    private static int ACTION_1_DATA = 0;

    @SettingLoad("shop.aktion8.itemname")
    private static String ACTION_8_MATN = Material.GOLD_BLOCK.toString();
    @SettingLoad("shop.aktion8.itemdata")
    private static int ACTION_8_DATA = 0;

    @SettingLoad("shop.aktion64.itemname")
    private static String ACTION_64_MATN = Material.EMERALD_BLOCK.toString();
    @SettingLoad("shop.aktion64.itemdata")
    private static int ACTION_64_DATA = 0;
    
    private static final String CONT_CONTENT = "cont_content";
    private static final String BTN_AMOUNT1 = "btn_amount1";
    private static final String BTN_AMOUNT8 = "btn_amount8";
    private static final String BTN_AMOUNT64 = "btn_amount64";
    
    private Market parent;
    private Chest shopChest;
    private ShopState state;

    private ItemStack itemOfInterest;
    private double price = -1;
    private int neededAmount = -1; // Only used when in buy mode
    private Item droppedItem;

    private Window<NodeContainer> root;
    
    public Shop(Market parent, Block shopChest, ItemStack[] shopContents) {
        if (shopChest.getType() != Material.CHEST) {
            throw new IllegalStateException("You're not allowed to create a shop out of a non-chest block.");
        }
        this.parent = parent;
        this.shopChest = (Chest) shopChest.getState();
        this.state = ShopState.NOTHING;
        
        this.root = RoninMarket.WINDOW.createNodeWindow(9, 3, true);
        this.root.getContainer().nodeCreator().button(0, 0, this.createButton(ACTION_1_MATN, ACTION_1_DATA, 1), BTN_AMOUNT1)
                .registerListener(instance -> this.action(instance, 1));
        this.root.getContainer().nodeCreator().button(0, 1, this.createButton(ACTION_8_MATN, ACTION_8_DATA, 8), BTN_AMOUNT8)
                .registerListener(instance -> this.action(instance, 8));
        this.root.getContainer().nodeCreator().button(0, 2, this.createButton(ACTION_64_MATN, ACTION_64_DATA, 64), BTN_AMOUNT64)
                .registerListener(instance -> this.action(instance, 64));
    
        Window<StorageContainer> contents = RoninMarket.WINDOW.createStorageWindow(8, 3, true);
        for (ItemStack is : shopContents) {
            if (is != null) contents.getContainer().add(is);
        }
        contents.getContainer().registerFilter(this::shopFilter);
        contents.registerCloseListener(((root1, instance, type) -> {
            if (type == Window.CloseType.POP && instance.getPlayer().getUniqueId().equals(this.parent.getOwner())) {
                RoninMarket.getInstance().getShopManager().saveShop(this);
            }
        }));
        this.root.addChild(contents, 1, 0, CONT_CONTENT);
        this.root.setChildMoveHandler(CONT_CONTENT);
    }

    private FilterResult shopFilter(WindowInstance<? extends Container> root, WindowInstance<StorageContainer> instance, FilterEventType type, FilterEvent event) {
        if (!root.getPlayer().getUniqueId().equals(this.parent.getOwner())) {
            return FilterResult.BLOCK; // Only the shop owner can edit its content
        }
        
        // Only allow the item of interest to be put inside
        if (event.getItem().isSimilar(this.itemOfInterest)) {
            return FilterResult.ALLOW;
        } else {
            return FilterResult.BLOCK;
        }
    }
    
    private void action(WindowInstance<NodeContainer> instance, int amount) {
        switch (this.state) {
            case BUY:
                this.actionBuy(instance, amount);
                break;
            case SELL:
                this.actionSell(instance, amount);
                break;
        }
    }
    
    private void actionBuy(WindowInstance<NodeContainer> instance, int amount) {
        WindowInstance.WindowInstanceChild<StorageContainer> contentChild = instance.searchChild(CONT_CONTENT);
        int neededAmount = Math.min(this.neededAmount - contentChild.child.getContainer().count(this.itemOfInterest), amount);
        if (neededAmount <= 0) {
            instance.getPlayer().sendMessage("\u00a7cDer Besitzer kauft nicht so viele Items an.");
            return;
        }
        
        int actualAmount = Math.min(InventoryHelper.countItem(instance.getPlayer().getInventory().getStorageContents(), this.itemOfInterest), neededAmount);
        if (actualAmount <= 0) {
            instance.getPlayer().sendMessage("\u00a7cDu hast keine Items zum Verkaufen.");
            return;
        }
        
        double totalPrice = this.price * actualAmount;
        RoninMarket.CONCURRENT.submitTask(() -> {
            Account shopAcc = null;
            while (shopAcc == null) {
                shopAcc = RoninMarket.ECONOMY.getAccount(this.parent.getOwner(), RoninMarket.CURRENCY);
            }
            Account sellerAcc = RoninMarket.ECONOMY.getAccount(instance.getPlayer().getUniqueId(), RoninMarket.CURRENCY);
    
            switch (shopAcc.transfer(sellerAcc, totalPrice)) {
                case SUCCESS:
                    ItemStack sold = this.itemOfInterest.clone();
                    sold.setAmount(actualAmount);
                    
                    contentChild.child.getContainer().add(sold);
                    
                    ItemStack[] playerContents = instance.getPlayer().getInventory().getStorageContents();
                    InventoryHelper.removeItem(playerContents, this.itemOfInterest, actualAmount);
                    instance.getPlayer().getInventory().setStorageContents(playerContents);
                    break;
                case NOT_ENOUGH_MONEY:
                    instance.getPlayer().sendMessage("\u00a7cDu kannst momentan nichts verkaufen, da der Besitzer zu wenig Geld hat.");
                    break;
                case OUT_OF_SPACE:
                    instance.getPlayer().sendMessage("\u00a7cDein Bankkonto ist voll.");
                    break;
            }
        });
    }
    
    private void actionSell(WindowInstance<NodeContainer> instance, int amount) {
        WindowInstance.WindowInstanceChild<StorageContainer> contentChild = instance.searchChild(CONT_CONTENT);
        int actualAmount = Math.min(contentChild.child.getContainer().count(this.itemOfInterest), amount);
        
        int availableSpace = InventoryHelper.countFreeSpace(instance.getPlayer().getInventory().getStorageContents(), this.itemOfInterest);
        if (actualAmount > availableSpace) {
            instance.getPlayer().sendMessage("\u00a7cDein Inventar ist zu voll, um so viele Items zu kaufen.");
            return;
        }
        
        double totalPrice = this.price * actualAmount;
        RoninMarket.CONCURRENT.submitTask(() -> {
            Account shopAcc = null;
            while (shopAcc == null) {
                shopAcc = RoninMarket.ECONOMY.getAccount(this.parent.getOwner(), RoninMarket.CURRENCY);
            }
            Account buyerAcc = RoninMarket.ECONOMY.getAccount(instance.getPlayer().getUniqueId(), RoninMarket.CURRENCY);
            switch (buyerAcc.transfer(shopAcc, totalPrice)) {
                case SUCCESS:
                    ItemStack bought = this.itemOfInterest.clone();
                    bought.setAmount(actualAmount);
                    contentChild.child.getContainer().remove(bought);
                    instance.getPlayer().getInventory().addItem(bought);
                    break;
                case NOT_ENOUGH_MONEY:
                    instance.getPlayer().sendMessage("\u00a7cDu hast nicht genug " + RoninMarket.CURRENCY.getName());
                    break;
                case OUT_OF_SPACE:
                    instance.getPlayer().sendMessage("\u00a7cDer Besitzer kann nicht diese Menge " + RoninMarket.CURRENCY.getName() + " nicht aufnehmen.");
                    break;
            }
        });
    }
    
    private void updateButtons() {
        Button amount1 = this.root.getContainer().getNode(BTN_AMOUNT1);
        amount1.setIcon(this.createButton(ACTION_1_MATN, ACTION_1_DATA, 1));
        Button amount8 = this.root.getContainer().getNode(BTN_AMOUNT8);
        amount8.setIcon(this.createButton(ACTION_8_MATN, ACTION_8_DATA, 8));
        Button amount64 = this.root.getContainer().getNode(BTN_AMOUNT64);
        amount64.setIcon(this.createButton(ACTION_64_MATN, ACTION_64_DATA, 64));
    }
    
    /* PACKAGE PRIVATE */
    void activateBuyMode(ItemStack buy, int neededAmount, double price) {
        this.setItemOfInterest(buy);
        this.neededAmount = neededAmount;
        this.price = price;
        this.state = ShopState.BUY;
        
        this.updateButtons();
    }

    void activeSellMode(ItemStack sell, double price) {
        this.setItemOfInterest(sell);
        this.price = price;
        this.state = ShopState.SELL;
    
        this.updateButtons();
    }
    
    public void resetState() {
        this.setItemOfInterest(null);
        this.price = -1;
        this.neededAmount = -1;
        this.state = ShopState.NOTHING;
    
        this.updateButtons();
    }

    public void openInventory(Player player) {
        String title;
        if (this.state == ShopState.SELL) {
            title = "\u00a72" + this.itemOfInterest.getType().name() + " wird verkauft";
        } else {
            title = "\u00a72" + this.neededAmount + "x" + this.itemOfInterest.getType().name() + " wird gekauft";
        }
        RoninMarket.WINDOW.getContext(player).open(this.root, InventoryType.CHEST, title);
    }

    public void despawnDroppedItem() {
        if (this.droppedItem != null) {
            this.droppedItem.remove();
        }
    }

    public void onDestroy() {
        this.despawnDroppedItem();
        World world = this.shopChest.getWorld();
        Location location = this.shopChest.getLocation().add(0.5, 0.5, 0.5);
        for (ItemStack item : this.getShopContents()) {
            if (item != null) world.dropItem(location, item);
        }
    }

    private void setItemOfInterest(ItemStack itemOfInterest) {
        this.despawnDroppedItem();
        this.itemOfInterest = itemOfInterest;
        if (itemOfInterest == null) return;
        ItemStack drop = this.itemOfInterest.clone();
        drop.setAmount(1);
        this.droppedItem = this.shopChest.getWorld().dropItem(this.shopChest.getLocation().clone().add(0.5, 1.5, 0.5), drop);
        this.droppedItem.setCustomNameVisible(true);
        this.droppedItem.setPickupDelay(Integer.MAX_VALUE);
        this.droppedItem.setVelocity(new Vector(0, 0, 0));
        this.droppedItem.setMetadata("_MARKET_NOPICKUP_", new FixedMetadataValue(
                RoninMarket.getPlugin(RoninMarket.class),
                null
        ));
    }

    private ItemStack createButton(String name, int data, int count) {
        return RoninMarket.ITEM.builder(Material.valueOf(name), (byte) data)
                .name("\u00a74" + count + " Stück " + (this.state == ShopState.BUY ? "verkaufen" : "kaufen"))
                .addLore("\u00a7c" + RoninMarket.CURRENCY.formatAmount(count * this.price))
                .finish();
    }

    public void clearShopContents() {
        Window.WindowChild<StorageContainer> childContent = this.root.searchChild(CONT_CONTENT);
        childContent.child.getContainer().clear();
    }
    
    public Market getParent() { return this.parent; }
    public Chest getChest() { return this.shopChest; }
    public ShopState getState() { return this.state; }
    public ItemStack getItemOfInterest() { return this.itemOfInterest; }
    public int getNeededAmount() { return this.neededAmount; }
    public double getPrice() { return this.price; }
    
    public ItemStack[] getShopContents() {
        Window.WindowChild<StorageContainer> childContent = this.root.searchChild(CONT_CONTENT);
        return childContent.child.getContainer().getContents();
    }
}
