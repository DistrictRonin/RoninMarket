package de.ostylk.roninmarket.market.shop;

import de.ostylk.baseapi.modules.config.Config;
import de.ostylk.roninmarket.RoninMarket;
import de.ostylk.roninmarket.market.Market;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import java.util.HashSet;
import java.util.Set;

public class ShopManager {

    private final Config backend;
    private final Set<Shop> shops;

    public ShopManager(Plugin plugin) {
        this.backend = RoninMarket.CONFIG.loadConfig(plugin, "shops");
        this.shops = new HashSet<>();
        this.load();
    }

    private void load() {
        this.backend.getSectionEntries("shops").forEach(shopKey -> {
            Shop shop = (Shop) this.backend.get("shops." + shopKey);
            this.shops.add(shop);
        });
    }

    private String encodeLocation(Location loc) {
        return loc.getBlockX() + ";" + loc.getBlockY() + ";" + loc.getBlockZ() + ";" + loc.getWorld().getName();
    }

    public void notifyServerShutdown() {
        this.shops.forEach(Shop::despawnDroppedItem);
    }

    public void saveShop(Shop shop) {
        this.backend.set("shops." + this.encodeLocation(shop.getChest().getLocation()), shop);
        this.backend.save();
    }

    public void sell(Shop shop, ItemStack is, double price) {
        shop.activeSellMode(is, price);
        this.backend.set("shops." + this.encodeLocation(shop.getChest().getLocation()), shop);
        this.backend.save();
    }

    public void buy(Shop shop, ItemStack is, int neededAmount, double price) {
        shop.activateBuyMode(is, neededAmount, price);
        this.backend.set("shops." + this.encodeLocation(shop.getChest().getLocation()), shop);
        this.backend.save();
    }

    public void createShop(Market market, Block shopChest) {
        Shop shop = new ShopSerializer(market, shopChest, new ItemStack[43]);
        this.shops.add(shop);
        this.backend.set("shops." + this.encodeLocation(shop.getChest().getLocation()), shop);
        this.backend.save();
    }

    public Shop getShop(Block block) {
        for (Shop shop : this.shops) {
            if (shop.getChest().getX() == block.getX() &&
                    shop.getChest().getY() == block.getY() &&
                    shop.getChest().getZ() == block.getZ() &&
                    shop.getChest().getWorld().getName().equals(block.getWorld().getName())) {
                return shop;
            }
        }
        return null;
    }

    public void removeShop(Shop shop) {
        this.shops.remove(shop);
        shop.onDestroy();
        this.backend.set("shops." + this.encodeLocation(shop.getChest().getLocation()), null);
        this.backend.save();
    }

    public Set<Shop> getShops() { return this.shops; }
}
