package de.ostylk.roninmarket.market.shop;

import de.ostylk.roninmarket.market.Market;
import de.ostylk.roninmarket.market.MarketManager;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntitySpawnEvent;

public class ShopBuildListener implements Listener {

    private final MarketManager marketManager;
    private final ShopManager shopManager;

    public ShopBuildListener(MarketManager marketManager, ShopManager shopManager) {
        this.marketManager = marketManager;
        this.shopManager = shopManager;
    }

    // priority is highest because this plugin should allow building even if protected
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlace(BlockPlaceEvent e) {
        if (this.isBlockModAllowed(e.getBlock(), e.getPlayer())) {
            e.setCancelled(false);
            if (e.getBlock().getType() == Material.CHEST) {
                if (this.checkDoublechest(e.getBlock())) {
                    e.getPlayer().sendMessage("\u00a7cDoppelkisten sind nicht erlaubt.");
                    e.setCancelled(true);
                    return;
                }

                Market market = this.marketManager.getMarket(e.getBlock().getLocation());
                this.shopManager.createShop(market, e.getBlock());
            } else {
                switch (e.getBlock().getType()) {
                    case LAVA:
                    case WATER:
                    case TNT:
                    case END_PORTAL:
                    case END_PORTAL_FRAME:
                        e.getPlayer().sendMessage("\u00a7cDiese Blöcke sind nicht erlaubt.");
                        return;
                }
                if (this.checkChestBelow(e.getBlock()) && !this.isAllowedBlock(e.getBlock())) {
                    e.getPlayer().sendMessage("\u00a7cDu darfst keine Blöcke über Kisten platzieren.");
                    e.setCancelled(true);
                    return;
                }
            }
        }
    }

    @EventHandler
    public void onSpawn(EntitySpawnEvent e) {
        if (e.getEntity().getType() == EntityType.ENDER_CRYSTAL) {
            Market market = this.marketManager.getMarket(e.getLocation());
            if (market != null) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBreak(BlockBreakEvent e) {
        if (this.isBlockModAllowed(e.getBlock(), e.getPlayer())) {
            e.setCancelled(false);
        }
    
        if (e.getBlock().getType() == Material.CHEST) {
            Shop shop = this.shopManager.getShop(e.getBlock());
            if (shop != null) {
                if (shop.getParent().getOwner() != null && e.getPlayer().getUniqueId().equals(shop.getParent().getOwner())) {
                    this.shopManager.removeShop(shop);
                    e.setCancelled(false);
                } else if (e.getPlayer().hasPermission("roninmarket.shop.force_destroy")) {
                    this.shopManager.removeShop(shop);
                    e.setCancelled(false);
                } else {
                    e.setCancelled(true);
                }
            }
        }
    }

    private boolean isBlockModAllowed(Block b, Player p) {
        Market market = this.marketManager.getMarket(b.getLocation());
        if (market != null && market.isOccupied()) {
            if (market.getOwner().equals(p.getUniqueId())) {
                return true;
            }
        }
        return false;
    }

    private boolean checkDoublechest(Block b) {
        Block[] toCheck = {
                b.getLocation().clone().add( 1, 0,  0).getBlock(),
                b.getLocation().clone().add( 0, 0,  1).getBlock(),
                b.getLocation().clone().add( 0, 0, -1).getBlock(),
                b.getLocation().clone().add(-1, 0,  0).getBlock()
        };
        for (Block block : toCheck) {
            if (block.getType() == Material.CHEST) {
                return true;
            }
        }
        return false;
    }

    private boolean checkChestBelow(Block b) {
        Block below = b.getLocation().clone().subtract(0, 1, 0).getBlock();
        return below.getType() == Material.CHEST;
    }

    private boolean isAllowedBlock(Block b) {
        return b.getType().toString().endsWith("_SIGN");
    }
}
