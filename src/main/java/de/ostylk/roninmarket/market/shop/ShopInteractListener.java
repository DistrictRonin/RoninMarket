package de.ostylk.roninmarket.market.shop;

import de.ostylk.roninmarket.RoninMarket;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.ItemDespawnEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

public class ShopInteractListener implements Listener {

    private final ShopManager shopManager;
    private final ShopChatInput chatInput;

    public ShopInteractListener(ShopManager shopManager, ShopChatInput input) {
        this.shopManager = shopManager;
        this.chatInput = input;
    }

    @EventHandler
    public void onPickup(PlayerPickupItemEvent e) {
        Item item = e.getItem();
        item.getMetadata("_MARKET_NOPICKUP_").stream().filter(value -> value.getOwningPlugin() == RoninMarket.getPlugin(RoninMarket.class)).forEach(value -> {
            e.setCancelled(true);
        });
    }

    @EventHandler
    public void onDespawn(ItemDespawnEvent e) {
        Item item = e.getEntity();
        item.getMetadata("_MARKET_NOPICKUP_").stream().filter(value -> value.getOwningPlugin() == RoninMarket.getPlugin(RoninMarket.class)).forEach(value -> {
            e.setCancelled(true);
        });
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        if (e.getHand() != EquipmentSlot.HAND) return;

        if (e.getAction() == Action.RIGHT_CLICK_BLOCK || e.getAction() == Action.LEFT_CLICK_BLOCK) {
            if (e.getClickedBlock().getType() == Material.CHEST) {
                Shop shop = this.shopManager.getShop(e.getClickedBlock());
                if (shop != null) {
                    boolean rightClick = e.getAction() == Action.RIGHT_CLICK_BLOCK;
                    if (rightClick) {
                        e.setCancelled(true);
                    }
                    this.handleShopClick(shop, e.getPlayer(), rightClick);
                }
            }
        }
    }

    private void handleShopClick(Shop shop, Player p, boolean rightClick) {
        if (!shop.getParent().isOccupied()) return;
        switch (shop.getState()) {
            case NOTHING:
                if (shop.getParent().getOwner().equals(p.getUniqueId())) {
                    this.handleShopSet(shop, p, rightClick);
                }
                break;
            case BUY:
            case SELL:
                if (!rightClick) return;
                shop.openInventory(p);
                break;
        }
    }

    private void handleShopSet(Shop shop, Player owner, boolean rightClick) {
        if (owner.getInventory().getItemInMainHand().getType() == Material.AIR) return;
        if (rightClick) {
            // Owner wants to sell his item
            ItemStack toSell = owner.getInventory().getItemInMainHand().clone();
            owner.sendMessage("\u00a76Geb bitte an für wie viel du 1 Item verkaufen möchtest.");
            this.chatInput.registerInput(owner, price -> {
                if (price <= 0) {
                    owner.sendMessage("\u00a7cDu musst eine Zahl größer als 0 eingeben.");
                    owner.sendMessage("\u00a7cAbgebrochen");
                    return;
                }

                Bukkit.getScheduler().runTask(RoninMarket.getPlugin(RoninMarket.class), () -> {
                    this.shopManager.sell(shop, toSell, price);
                });
            });
        } else {
            // Owner wants to buy that kind of item
            ItemStack toBuy = owner.getInventory().getItemInMainHand().clone();
            owner.sendMessage("\u00a76Geb bitte an wie viel du ankaufen möchtest.");
            this.chatInput.registerInput(owner, count -> {
                if (count <= 0) {
                    owner.sendMessage("\u00a7cDu musst eine Zahl größer als 0 eingeben.");
                    owner.sendMessage("\u00a7cAbgebrochen");
                    return;
                }

                final int STACKS = shop.getChest().getInventory().getSize() - 3;
                final int MAX_AMOUNT = STACKS * toBuy.getMaxStackSize();
                if (count > MAX_AMOUNT) {
                    Bukkit.getPlayer(shop.getParent().getOwner()).sendMessage("\u00a7cDu darfst maximal " + MAX_AMOUNT + " von diesem Item ankaufen.");
                    owner.sendMessage("\u00a7cAbgebrochen");
                    return;
                }

                owner.sendMessage("\u00a76Geb bitte an für wie viel du 1 Item ankaufen möchtest.");
                this.chatInput.registerInput(owner, price -> {
                    if (count <= 0) {
                        owner.sendMessage("\u00a7cDu musst eine Zahl größer als 0 eingeben.");
                        owner.sendMessage("\u00a7cAbgrebrochen");
                        return;
                    }

                    // We need to sync back to bukkit main thread
                    Bukkit.getScheduler().runTask(RoninMarket.getPlugin(RoninMarket.class), () -> {
                        this.shopManager.buy(shop, toBuy, count.intValue(), price);
                    });
                });
            });
        }
    }
}
