package de.ostylk.roninmarket.market.shop;

import de.ostylk.baseapi.modules.config.settings.SettingLoad;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

public class ShopChatInput implements Listener {

    @SettingLoad("chat.max_input")
    private static double MAX_INPUT = 1000.0D;
    
    private final Map<String, Consumer<Double>> readFromChat;

    public ShopChatInput() {
        this.readFromChat = new HashMap<>();
    }

    public void registerInput(Player p, Consumer<Double> callback) {
        this.readFromChat.put(p.getName(), callback);
        p.sendMessage("\u00a76Mit '-' kannst du dies jederzeit abbrechen.");
    }

    @EventHandler
    public void onAsyncChat(AsyncPlayerChatEvent e) {
        if (this.readFromChat.containsKey(e.getPlayer().getName())) {
            e.setCancelled(true);
            if (e.getMessage().trim().equals("-")) {
                e.getPlayer().sendMessage("\u00a7cAbgebrochen");
                this.readFromChat.remove(e.getPlayer().getName());
                return;
            }

            try {
                BigDecimal decimal = new BigDecimal(e.getMessage());
                decimal = decimal.setScale(2, RoundingMode.UP);
                if (decimal.doubleValue() > MAX_INPUT) {
                    e.getPlayer().sendMessage("\u00a7cDie Zahl ist zu groß. Du darfst maximal " + MAX_INPUT + " eingeben.");
                    e.getPlayer().sendMessage("\u00a7cVersuch es nochmal.");
                    return;
                }
                Consumer<Double> callback = this.readFromChat.remove(e.getPlayer().getName());
                callback.accept(decimal.doubleValue());
            } catch (NumberFormatException ex) {
                e.getPlayer().sendMessage("\u00a7cDas ist keine Zahl. Versuch es nochmal.");
            }
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        this.readFromChat.remove(e.getPlayer().getName());
    }
}
