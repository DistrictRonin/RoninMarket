package de.ostylk.roninmarket.market.shop;

public enum ShopState {

    SELL(0), BUY(1), NOTHING(2);

    private final int type;

    ShopState(int type) {
        this.type = type;
    }

    public int getType() { return this.type; }

    public static ShopState getFromType(int type) {
        for (ShopState state : ShopState.values()) {
            if (state.getType() == type) {
                return state;
            }
        }
        throw new IllegalArgumentException("This ShopState doesn't exist");
    }
}
