package de.ostylk.roninmarket.market.shop;

import de.ostylk.roninmarket.RoninMarket;
import de.ostylk.roninmarket.market.Market;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;

public class ShopSerializer extends Shop implements ConfigurationSerializable {

    public ShopSerializer(Market parent, Block shopChest, ItemStack[] shopContents) {
        super(parent, shopChest, shopContents);
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> data = new HashMap<>();

        data.put("location", this.getChest().getLocation());
        data.put("parent", this.getParent().getID());
        data.put("state", this.getState().getType());

        Map<String, Object> contents = new HashMap<>();
        for (int i = 0; i < this.getShopContents().length; i++) {
            ItemStack is = this.getShopContents()[i];
            if (is == null) continue;
            contents.put(String.valueOf(i), is);
        }
        data.put("contents", contents);
        if (this.getState() != ShopState.NOTHING) {
            Map<String, Object> auction = new HashMap<>();

            auction.put("item", this.getItemOfInterest());
            auction.put("price", this.getPrice());
            auction.put("neededAmount", this.getNeededAmount());

            data.put("auction", auction);
        }

        return data;
    }

    public static ShopSerializer deserialize(Map<String, Object> data) {
        Location location = (Location) data.get("location");
        Market parent = RoninMarket.getPlugin(RoninMarket.class).getMarketManager().getMarket((int) data.get("parent"));
        ShopState state = ShopState.getFromType((int) data.get("state"));

        Map<String, Object> contents = (Map<String, Object>) data.get("contents");
        ItemStack[] shopContents = new ItemStack[43];
        contents.keySet().forEach(key -> {
            int idx = Integer.parseInt(key);
            shopContents[idx] = (ItemStack) contents.get(key);
        });

        ShopSerializer shop = new ShopSerializer(parent, location.getBlock(), shopContents);
        Map<String, Object> auction = (Map<String, Object>) data.get("auction");
        switch (state) {
            case BUY:
                ItemStack buyItem = (ItemStack) auction.get("item");
                double buyPrice = (double) auction.get("price");
                int neededAmount = (int) auction.get("neededAmount");
                shop.activateBuyMode(buyItem, neededAmount, buyPrice);
                break;
            case SELL:
                ItemStack sellItem = (ItemStack) auction.get("item");
                double sellPrice = (double) auction.get("price");
                shop.activeSellMode(sellItem, sellPrice);
        }
        return shop;
    }

    /*private MarketManager marketManager;

    public ShopSerializer(MarketManager marketManager) {
        this.marketManager = marketManager;
    }

    @Override
    public void set(Config config, String prefix, Shop value) {
        config.set(prefix + ".location", value.getChest().getLocation());
        config.set(prefix + ".parent", value.getParent().getID());
        config.set(prefix + ".state", value.getState().getType());
        if (value.getState() == ShopState.NOTHING) {
            config.set(prefix + ".auction", null);
        } else {
            config.set(prefix + ".auction.item", value.getItemOfInterest().clone());
            config.set(prefix + ".auction.price", value.getPrice());
            config.set(prefix + ".auction.neededAmount", value.getNeededAmount());
        }
    }

    @Override
    public Shop get(Config config, String prefix) {
        Location location = (Location) config.get(prefix + ".location");
        Market market = this.marketManager.getMarket((int) config.get(prefix + ".parent"));
        ShopState state = ShopState.getFromType((int) config.get(prefix + ".state"));

        Shop shop = new Shop(market, location.getBlock());
        switch (state) {
            case BUY:
                ItemStack buyItem = (ItemStack) config.get(prefix + ".auction.item");
                double buyPrice = (double) config.get(prefix + ".auction.price");
                int neededAmount = (int) config.get(prefix + ".auction.neededAmount");
                shop.activateBuyMode(buyItem, neededAmount, buyPrice);
                break;
            case SELL:
                ItemStack sellItem = (ItemStack) config.get(prefix + ".auction.item");
                double sellPrice = (double) config.get(prefix + ".auction.price");
                shop.activeSellMode(sellItem, sellPrice);
                break;
        }
        return shop;
    }

    @Override
    public String getTypeName() {
        return Shop.class.getSimpleName();
    }*/
}
