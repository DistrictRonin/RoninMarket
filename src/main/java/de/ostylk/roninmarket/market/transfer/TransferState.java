package de.ostylk.roninmarket.market.transfer;

public enum TransferState {
    WAIT_TARGETNAME,
    WAIT_OWNER_CONFIRM,
    WAIT_TARGET_CONFIRM
}
