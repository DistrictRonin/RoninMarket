package de.ostylk.roninmarket.market.transfer;

import de.ostylk.roninmarket.market.MarketManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class TransferListener implements Listener {

    private final MarketManager marketManager;

    public TransferListener(MarketManager marketManager) {
        this.marketManager = marketManager;
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {
        TransferInfo info = this.marketManager.getTransferManager().getInfo(e.getPlayer());
        if (info == null) return;
        switch (info.getState()) {
            case WAIT_TARGETNAME:
                if (info.getMarket().getOwnerName().equals(e.getPlayer().getName())) {
                    e.setCancelled(true);
                    if (e.getMessage().trim().equals("-")) {
                        this.marketManager.getTransferManager().cancelTransfer(e.getPlayer());
                        return;
                    }
                    Player target = Bukkit.getPlayer(e.getMessage());
                    if (target != null) {
                        this.marketManager.getTransferManager().setTargetName(info.getMarket(), target);
                    } else {
                        e.getPlayer().sendMessage("\u00a7cDieser Spieler ist nicht online. Vielleicht vertippt?");
                    }
                }
                break;
        }
    }
}
