package de.ostylk.roninmarket.market.transfer;

import de.ostylk.roninmarket.market.Market;
import org.bukkit.entity.Player;

public class TransferInfo {

    private final Market market;
    private Player target;
    private TransferState state;

    public TransferInfo(Market market) {
        this.market = market;
        this.state = TransferState.WAIT_TARGETNAME;
    }

    public void setState(TransferState state) { this.state = state; }
    public void setTarget(Player target) { this.target = target; }

    public Market getMarket() { return this.market; }
    public Player getTarget() { return this.target; }
    public TransferState getState() { return this.state; }
}
