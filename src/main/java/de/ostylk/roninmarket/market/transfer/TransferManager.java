package de.ostylk.roninmarket.market.transfer;

import de.ostylk.roninmarket.market.Market;
import de.ostylk.roninmarket.market.MarketManager;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

public class TransferManager {

    private final MarketManager marketManager;
    private final Map<String, TransferInfo> transferProcess;

    public TransferManager(MarketManager marketManager) {
        this.marketManager = marketManager;
        this.transferProcess = new HashMap<>();
    }

    public void notifyPlayerOffline(Player p) {
        cancelTransfer(p);
    }

    public void cancelTransfer(Player p) {
        TransferInfo info = this.transferProcess.get(p.getName());
        if (info != null) {
            this.transferProcess.remove(info.getMarket().getOwnerName());
            if (info.getTarget() != null) {
                this.transferProcess.remove(info.getTarget().getName());
            }

            Player owner = Bukkit.getPlayer(info.getMarket().getOwner());
            owner.sendMessage("\u00a7cAktion abgebrochen.");
            if (info.getTarget() != null) {
                info.getTarget().sendMessage("\u00a7cAktion abgebrochen");
            }
        }
    }

    public void beginTransfer(Market market) {
        TransferInfo info = new TransferInfo(market);
        this.transferProcess.put(market.getOwnerName(), info);

        Player owner = Bukkit.getPlayer(market.getOwner());
        owner.sendMessage("\u00a77Geb den Namen von dem Spieler ein an den der Markt übertragen werden soll.");
        owner.sendMessage("\u00a77Mit '-' kannst du den Vorgang abbrechen.");
    }

    public void setTargetName(Market market, Player target) {
        TransferInfo info = this.transferProcess.get(market.getOwnerName());
        info.setState(TransferState.WAIT_OWNER_CONFIRM);
        info.setTarget(target);
        this.transferProcess.put(market.getOwnerName(), info);

        Player owner = Bukkit.getPlayer(market.getOwner());
        long daysRemaining = (market.getExpireDate() - System.currentTimeMillis()) / 1000 / 60 / 60 / 24 + 1;
        owner.sendMessage("\u00a76Der Pachtvertrag läuft noch " + daysRemaining + " Tage.");
        owner.sendMessage("\u00a76Möchtest du den Markt an " + target.getName() + " überschreiben?");

        ComponentBuilder positive = new ComponentBuilder("[Bestätigen]");
        positive.color(ChatColor.GREEN);
        positive.event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/mt accept"));
        ComponentBuilder negative = new ComponentBuilder("[Abbrechen]");
        negative.color(ChatColor.RED);
        negative.event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/mt decline"));
        owner.spigot().sendMessage(positive.create());
        owner.spigot().sendMessage(negative.create());
    }

    public void confirmOwner(Market market) {
        TransferInfo info = this.transferProcess.get(market.getOwnerName());
        info.setState(TransferState.WAIT_TARGET_CONFIRM);
        this.transferProcess.put(market.getOwnerName(), info);
        this.transferProcess.put(info.getTarget().getName(), info);

        Player owner = Bukkit.getPlayer(market.getOwner());
        owner.sendMessage("\u00a7aBestätigt.");

        long daysRemaining = (market.getExpireDate() - System.currentTimeMillis()) / 1000 / 60 / 60 / 24 + 1;
        info.getTarget().sendMessage("\u00a76Möchtest du den Markt von " + market.getOwnerName() + " übernehmen?");
        info.getTarget().sendMessage("\u00a76Der Pachtvertrag läuft noch " + daysRemaining + " Tage.");
        ComponentBuilder positive = new ComponentBuilder("[Bestätigen]");
        positive.color(ChatColor.GREEN);
        positive.event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/mt accept"));
        ComponentBuilder negative = new ComponentBuilder("[Abbrechen]");
        negative.color(ChatColor.RED);
        negative.event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/mt decline"));
        info.getTarget().spigot().sendMessage(positive.create());
        info.getTarget().spigot().sendMessage(negative.create());
    }

    public void confirmTarget(Player p) {
        TransferInfo info = this.transferProcess.get(p.getName());
    
        Player owner = Bukkit.getPlayer(info.getMarket().getOwner());
        owner.sendMessage("\u00a7aDein Markt wurde an " + info.getTarget().getName() + " übertragen.");
        info.getTarget().sendMessage("\u00a7aDu hast den Markt von " + owner.getName() + " erhalten.");
    
        this.marketManager.transferMarket(info.getMarket(), info.getTarget());
        
        this.transferProcess.remove(owner.getName());
        this.transferProcess.remove(info.getTarget().getName());
    }

    public TransferInfo getInfo(Player p) {
        return this.transferProcess.get(p.getName());
    }
}
