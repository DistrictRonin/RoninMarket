package de.ostylk.roninmarket.market;

import de.ostylk.roninmarket.market.tracker.MarketRegionEnter;
import net.minecraft.server.v1_14_R1.IChatBaseComponent;
import net.minecraft.server.v1_14_R1.PacketPlayOutTitle;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class MarketRegionTitle implements Listener {

    @EventHandler
    public void onEnter(MarketRegionEnter e) {
        if (e.getMarket().isOccupied()) {
            sendTitle(e.getPlayer(), "\u00a76" + e.getMarket().getOwnerName() + "'s Markt", "\u00a76Markt " + e.getMarket().getID());
        } else {
            sendTitle(e.getPlayer(), "\u00a76Leerstehender Markt", "\u00a76Markt " + e.getMarket().getID());
        }
    }

    private void sendTitle(Player p, String header, String subheader) {
        IChatBaseComponent titleChat = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + header + "\"}");
        PacketPlayOutTitle title = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TITLE, titleChat,
                2, 20, 2);
        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(title);

        IChatBaseComponent sutitleChat = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + subheader + "\"}");
        PacketPlayOutTitle subtitle = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.SUBTITLE, sutitleChat,
                2, 20, 2);
        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(subtitle);
    }
}
