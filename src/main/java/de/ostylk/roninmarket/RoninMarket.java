package de.ostylk.roninmarket;

import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import de.ostylk.baseapi.BaseAPI;
import de.ostylk.baseapi.modules.command.ModuleCommand;
import de.ostylk.baseapi.modules.concurrent.ModuleConcurrent;
import de.ostylk.baseapi.modules.config.ModuleConfig;
import de.ostylk.baseapi.modules.config.settings.SettingManager;
import de.ostylk.baseapi.modules.economy.Currency;
import de.ostylk.baseapi.modules.economy.ModuleEconomy;
import de.ostylk.baseapi.modules.item.ModuleItem;
import de.ostylk.baseapi.modules.window.ModuleWindow;
import de.ostylk.roninmarket.market.Market;
import de.ostylk.roninmarket.market.MarketManager;
import de.ostylk.roninmarket.market.MarketRegionTitle;
import de.ostylk.roninmarket.market.MarketSerializer;
import de.ostylk.roninmarket.market.shop.*;
import de.ostylk.roninmarket.market.tracker.MarketRegionTracker;
import de.ostylk.roninmarket.market.transfer.TransferListener;
import de.ostylk.roninmarket.marketchief.MarketChiefListener;
import de.ostylk.roninmarket.marketchief.MarketChiefManager;
import de.ostylk.roninmarket.marketchief.MarketChiefSerializer;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class RoninMarket extends JavaPlugin {

    public static ModuleEconomy ECONOMY;
    public static ModuleConcurrent CONCURRENT;
    public static ModuleConfig CONFIG;
    public static ModuleItem ITEM;
    public static ModuleWindow WINDOW;

    public static Currency CURRENCY;

    private MarketManager marketManager;
    private MarketChiefManager marketChiefManager;
    private ShopManager shopManager;

    private WorldEditPlugin wePlugin;

    private final Runnable initPlugin = () -> {
        // Initialization of config serializers
        ConfigurationSerialization.registerClass(MarketChiefSerializer.class);
        ConfigurationSerialization.registerClass(ShopSerializer.class);
        ConfigurationSerialization.registerClass(MarketSerializer.class);

        // Early init of Managers
        this.marketManager = new MarketManager(this);
        this.marketChiefManager = new MarketChiefManager(this);

        // Running post init hooks
        this.shopManager = new ShopManager(this);
        this.marketChiefManager.postInit(this);

        this.wePlugin = (WorldEditPlugin) getServer().getPluginManager().getPlugin("WorldEdit");

        //getCommand("mt").setExecutor(new CommandMarket(this));

        ShopChatInput chatInput = new ShopChatInput();
        getServer().getPluginManager().registerEvents(new MarketRegionTracker(this.marketManager), this);
        getServer().getPluginManager().registerEvents(new MarketRegionTitle(), this);
        getServer().getPluginManager().registerEvents(new MarketChiefListener(this.marketChiefManager), this);
        getServer().getPluginManager().registerEvents(new TransferListener(this.marketManager), this);
        getServer().getPluginManager().registerEvents(new PlayerHelper(), this);
        getServer().getPluginManager().registerEvents(new ShopBuildListener(this.marketManager, this.shopManager), this);
        getServer().getPluginManager().registerEvents(chatInput, this);
        getServer().getPluginManager().registerEvents(new ShopInteractListener(this.shopManager, chatInput), this);

        getServer().getPluginManager().registerEvents(new Listener() {
            @EventHandler
            public void onQuit(PlayerQuitEvent e) {
                marketManager.notifyPlayerOffline(e.getPlayer());
            }
        }, this);
        getServer().getPluginManager().registerEvents(new Listener() {
            @EventHandler
            public void onJoin(PlayerJoinEvent e) {
                marketManager.notifyPlayerOnline(e.getPlayer());
            }
        }, this);

        getServer().getScheduler().runTaskTimerAsynchronously(this, () -> {
            this.marketManager.getMarkets().values().forEach(market -> {
                if (market.isOccupied() && market.hasExpired()) {
                    this.marketManager.unlendMarket(market);
                }
            });
        }, 20L, 20L);

        getServer().broadcastMessage("\u00a7aMärkte-Plugin gestartet...");
    };

    private void initSettings() {
        SettingManager settingManager = CONFIG.getSettingManager(this);

        settingManager.loadSettings(Market.class);
        settingManager.loadSettings(Shop.class);
        settingManager.loadSettings(ShopChatInput.class);
    }

    @Override
    public void onLoad() {
        BaseAPI baseAPI = this.loadServiceProvider(BaseAPI.class);
        baseAPI.getModule(ModuleCommand.class).scanPlugin(this);
    }

    @Override
    public void onEnable() {
        BaseAPI baseAPI = this.loadServiceProvider(BaseAPI.class);
        baseAPI.getModule(ModuleCommand.class).finishPlugin(this, "roninmarket.commands");

        ECONOMY = baseAPI.getModule(ModuleEconomy.class);
        CONCURRENT = baseAPI.getModule(ModuleConcurrent.class);
        CONFIG = baseAPI.getModule(ModuleConfig.class);
        ITEM = baseAPI.getModule(ModuleItem.class);
        WINDOW = baseAPI.getModule(ModuleWindow.class);

        getServer().getScheduler().runTaskLater(this, initPlugin, 20L * 5L);

        CURRENCY = ECONOMY.getCurrency("ronin_main");

        this.initSettings();
    }

    private <T> T loadServiceProvider(Class<T> service) {
        RegisteredServiceProvider<T> provider = getServer().getServicesManager().getRegistration(service);
        if (provider == null) {
            throw new IllegalStateException("Could not load provider for service.");
        }

        return provider.getProvider();
    }

    @Override
    public void onDisable() {
        this.shopManager.notifyServerShutdown();
    }

    public MarketManager getMarketManager() { return this.marketManager; }
    public MarketChiefManager getMarketChiefManager() { return this.marketChiefManager; }
    public ShopManager getShopManager() { return this.shopManager; }
    public WorldEditPlugin getWEPlugin() { return this.wePlugin; }

    public static RoninMarket getInstance() {
        return RoninMarket.getPlugin(RoninMarket.class);
    }
}
