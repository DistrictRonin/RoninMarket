package de.ostylk.roninmarket.math;

import org.bukkit.util.Vector;

public class AABB {

    private final Vector min;
    private final Vector max;

    public AABB(Vector a, Vector b) {
        this.min = new Vector(
                Math.min(a.getX(), b.getX()),
                Math.min(a.getY(), b.getY()),
                Math.min(a.getZ(), b.getZ())
        );
        this.max = new Vector(
                Math.max(a.getX(), b.getX()),
                Math.max(a.getY(), b.getY()),
                Math.max(a.getZ(), b.getZ())
        );
    }

    public String encode() {
        return min.getBlockX() + ";" + min.getBlockY() + ";" + min.getBlockZ() + "#"
                + max.getBlockX() + ";" + max.getBlockY() + ";" + max.getBlockZ();
    }

    public boolean isInside(Vector check) {
        if (check.getX() >= this.min.getX() && check.getX() < this.max.getX() + 1) {
            if (check.getY() >= this.min.getY() && check.getY() < this.max.getY() + 1) {
                if (check.getZ() >= this.min.getZ() && check.getZ() < this.max.getZ() + 1) {
                    return true;
                }
            }
        }
        return false;
    }

    public Vector getMin() { return this.min; }
    public Vector getMax() { return this.max; }

    public static AABB decode(String encodedAABB) {
        String[] minMaxS = encodedAABB.split("#");
        String[] minS = minMaxS[0].split(";");
        String[] maxS = minMaxS[1].split(";");

        Vector min = new Vector(
                Integer.parseInt(minS[0]),
                Integer.parseInt(minS[1]),
                Integer.parseInt(minS[2])
        );
        Vector max = new Vector(
                Integer.parseInt(maxS[0]),
                Integer.parseInt(maxS[1]),
                Integer.parseInt(maxS[2])
        );

        return new AABB(min, max);
    }
}
