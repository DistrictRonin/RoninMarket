package de.ostylk.roninmarket.marketchief;

import de.ostylk.baseapi.modules.config.Config;
import de.ostylk.roninmarket.RoninMarket;
import de.ostylk.roninmarket.market.Market;
import de.ostylk.roninmarket.util.BookUtility;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.plugin.Plugin;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MarketChiefManager {

    private Config backend;

    private Map<Integer, MarketChief> marketChiefs;

    private final ItemStack helpBook;

    public MarketChiefManager(Plugin plugin) {
        this.backend = RoninMarket.CONFIG.loadConfig(plugin, "marketchiefs");

        this.helpBook = new ItemStack(Material.WRITTEN_BOOK);
        this.loadHelpBook(new File(plugin.getDataFolder(), "helpbook.txt"));
    }

    private void loadHelpBook(File dataFile) {
        if (!dataFile.exists()) {
            try {
                dataFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        List<String> bookContent = new ArrayList<>();
        String line;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(dataFile));
            StringBuilder buffer = new StringBuilder();
            while (true) {
                line = reader.readLine();
                if (line == null || line.trim().equals("#~~#")) {
                    bookContent.add(buffer.toString().trim());
                    buffer.delete(0, buffer.length() - 1);
                    if (line == null) break;
                } else {
                    buffer.append(line).append('\n');
                }
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        BookMeta bookMeta = (BookMeta) this.helpBook.getItemMeta();
        bookMeta.setPages(bookContent);
        this.helpBook.setItemMeta(bookMeta);
    }

    public void postInit(RoninMarket plugin) {
        this.marketChiefs = new HashMap<>();
        this.backend.getSectionEntries("chiefs").forEach(chiefKey -> {
            MarketChief chief = (MarketChief) this.backend.get("chiefs." + chiefKey);
            this.marketChiefs.put(chief.getNpcID(), chief);
        });
    }

    public void openHelpBook(Player p) {
        p.closeInventory();
        BookUtility.openBook(this.helpBook, p);
    }

    public void update() {
        for (MarketChief chief : this.marketChiefs.values()) {
            chief.update();
        }
    }
    
    public void linkMarket(MarketChief chief, Market market, int slot) {
        chief.link(market, slot);
        this.backend.set("chiefs." + chief.getNpcID(), chief);
        this.backend.save();
    }

    public void unlinkMarket(MarketChief chief, Market market) {
        chief.unlink(market);
        this.backend.set("chiefs." + chief.getNpcID(), chief);
    }

    public void spawnMarketChief(String name, Location location) {
        MarketChief chief = new MarketChiefSerializer(name, location);
        this.marketChiefs.put(chief.getNpcID(), chief);
        this.backend.set("chiefs." + chief.getNpcID(), chief);
        this.backend.save();
    }

    public MarketChief getMarketChief(Entity e) {
        return this.marketChiefs.get(CitizensAPI.getNPCRegistry().getNPC(e).getId());
    }

    public void removeMarketChief(Entity e) {
        NPC npc = CitizensAPI.getNPCRegistry().getNPC(e);
        this.backend.set("chiefs." + npc.getId(), null);
        this.backend.save();

        MarketChief chief = this.marketChiefs.get(npc.getId());
        chief.despawn();
    }

    public Map<Integer, MarketChief> getMarketChiefs() { return this.marketChiefs; }
}
