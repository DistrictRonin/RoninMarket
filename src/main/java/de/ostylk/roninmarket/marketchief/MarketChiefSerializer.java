package de.ostylk.roninmarket.marketchief;

import de.ostylk.roninmarket.RoninMarket;
import org.bukkit.Location;
import org.bukkit.configuration.serialization.ConfigurationSerializable;

import java.util.HashMap;
import java.util.Map;

public class MarketChiefSerializer extends MarketChief implements ConfigurationSerializable {

    public MarketChiefSerializer(String name, Location location) {
        super(name, location);
    }

    public MarketChiefSerializer(String name, int npcID, MarketChiefLinkage linkage) {
        super(name, npcID, linkage);
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> data = new HashMap<>();

        data.put("name", this.getName());
        data.put("npcID", this.getNpcID());
        data.put("linkage", this.getLinkedMarkets().encode());

        return data;
    }

    public static MarketChiefSerializer deserialize(Map<String, Object> data) {
        String name = (String) data.get("name");
        int npcID = (int) data.get("npcID");
        MarketChiefLinkage linkage = MarketChiefLinkage.decode(
                RoninMarket.getPlugin(RoninMarket.class).getMarketManager(),
                (String) data.get("linkage")
        );

        return new MarketChiefSerializer(name, npcID, linkage);
    }

    /*private MarketManager marketManager;

    public MarketChiefSerializer(MarketManager marketManager) {
        this.marketManager = marketManager;
    }

    @Override
    public void set(Config config, String prefix, MarketChief value) {
        config.set(prefix + ".name", value.getName());
        config.set(prefix + ".npcID", value.getNpcID());
        config.set(prefix + ".linkage", value.getLinkedMarkets().encode());
        // TODO: Linkage should be another serializer instead of encoding it in a string
    }

    @Override
    public MarketChief get(Config config, String prefix) {
        String name = (String) config.get(prefix + ".name");
        int npcID = (int) config.get(prefix + ".npcID");
        MarketChiefLinkage linkage = MarketChiefLinkage.decode(this.marketManager, (String) config.get(prefix + ".linkage"));
        return new MarketChief(name, npcID, linkage);
    }

    @Override
    public String getTypeName() {
        return MarketChief.class.getSimpleName();
    }*/
}
