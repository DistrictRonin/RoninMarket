package de.ostylk.roninmarket.marketchief;

import de.ostylk.roninmarket.market.Market;
import de.ostylk.roninmarket.market.MarketManager;

import java.util.HashMap;
import java.util.Map;

public class MarketChiefLinkage {

    private final Map<Integer, Market> linkedMarkets;

    public MarketChiefLinkage() {
        this.linkedMarkets = new HashMap<>();
    }

    public void link(int slot, Market market) {
        this.linkedMarkets.put(slot, market);
    }

    public int unlink(Market market) {
        int slot = -1;
        for (Map.Entry<Integer, Market> entry : this.linkedMarkets.entrySet()) {
            if (entry.getValue().getID() == market.getID()) {
                slot = entry.getKey();
                break;
            }
        }
        if (slot != -1) {
            this.linkedMarkets.remove(slot);
        }
        return slot;
    }

    public String encode() {
        StringBuilder builder = new StringBuilder();
        this.linkedMarkets.forEach((slot, market) -> {
            builder.append(slot).append(";").append(market.getID()).append("#");
        });

        if (builder.length() == 0) {
            return "";
        } else {
            return builder.deleteCharAt(builder.length() - 1).toString();
        }
    }

    public Market getMarket(int slot) { return this.linkedMarkets.get(slot); }
    public Map<Integer, Market> getLinkedMarkets() { return this.linkedMarkets; }

    public static MarketChiefLinkage decode(MarketManager marketManager, String encoded) {
        MarketChiefLinkage linkage = new MarketChiefLinkage();

        String[] slotMarketPairs = encoded.split("#");
        if (slotMarketPairs.length == 0) return linkage;
        if (slotMarketPairs[0].trim().isEmpty()) return linkage;
        for (String slotMarketPair : slotMarketPairs) {
            String[] slotMarket = slotMarketPair.split(";");

            int slot = Integer.parseInt(slotMarket[0]);
            int marketID = Integer.parseInt(slotMarket[1]);
            linkage.link(slot, marketManager.getMarket(marketID));
        }

        return linkage;
    }
}
