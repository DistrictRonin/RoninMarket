package de.ostylk.roninmarket.marketchief;

import de.ostylk.baseapi.modules.window.Window;
import de.ostylk.baseapi.modules.window.node.NodeContainer;
import de.ostylk.baseapi.modules.window.node.elements.Button;
import de.ostylk.baseapi.modules.window.player.PlayerContext;
import de.ostylk.roninmarket.RoninMarket;
import de.ostylk.roninmarket.market.Market;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

public class MarketChiefWindow {

    private static final int WIDTH = 9;
    private static final int HEIGHT = 6;
    
    private final MarketChiefLinkage linkage;
    
    private final Window<NodeContainer> window;
    
    public MarketChiefWindow(MarketChiefLinkage linkage) {
        this.linkage = linkage;
        this.window = RoninMarket.WINDOW.createNodeWindow(WIDTH, HEIGHT, true);
        for (int i = 0; i < WIDTH * HEIGHT - 1; i++) {
            Market market = linkage.getMarket(i);
            Button marketButton = this.window.getContainer().nodeCreator().button(i % WIDTH, i / WIDTH,
                    market == null ? new ItemStack(Material.AIR) : this.buildItemStack(market),
                    "btn_" + i);
            final int marketButtonID = i;
            marketButton.registerListener(instance -> {
                Market actual = this.linkage.getMarket(marketButtonID);
                if (actual == null) return;
                if (!actual.isOccupied()) {
                    PlayerContext context = RoninMarket.WINDOW.getContext(instance.getPlayer());
                    context.openConfirmation("Markt pachten", player -> {
                            RoninMarket.getInstance().getMarketManager().lendMarket(player, actual);
                        }, "Abbrechen!", player -> {},
                        RoninMarket.ITEM.builder(Material.PAPER).name("\u00a7aKostet " + RoninMarket.CURRENCY.formatAmount(Market.EXTENSION_COST)).finish()
                    );
                } else {
                    if (!market.getOwner().equals(instance.getPlayer().getUniqueId())) return;
                    actual.openConfigWindow(instance.getPlayer());
                }
            });
        }
        Button help = this.window.getContainer().nodeCreator().button(WIDTH - 1, HEIGHT - 1,
                RoninMarket.ITEM.builder(Material.BOOK).name("\u00a76Hilfe").addLore("\u00a76Klicke hier für Hilfe").finish());
        help.registerListener(instance -> {
            RoninMarket.getInstance().getMarketChiefManager().openHelpBook(instance.getPlayer());
        });
    }
    
    private ItemStack buildItemStack(Market market) {
        boolean occupied = market.isOccupied();
        return RoninMarket.ITEM.builder(occupied ? Material.GOLD_BLOCK : Material.IRON_BLOCK)
                .name(occupied ? "\u00a76" + market.getOwnerName() + "s Markt" : "\u00a76Leerstehender Markt")
                .addLore("\u00a73Markt " + market.getID())
                .addLore(occupied ? "\u00a73Verpachtet bis " + market.getExpireDateFormatted() : "\u00a73Kann verpachtet werden")
                .finish();
    }
    
    public void update() {
        for (int i = 0; i < WIDTH * HEIGHT - 1; i++) {
            Market market = linkage.getMarket(i);
            Button marketButton = this.window.getContainer().getNode("btn_" + i);
            marketButton.setIcon(market == null ? new ItemStack(Material.AIR) : this.buildItemStack(market));
        }
    }
    
    public void link(Market market, int slot) {
        Button button = this.window.getContainer().getNode("btn_" + slot);
        button.setIcon(this.buildItemStack(market));
    }
    
    public void unlink(int slot) {
        if (slot == -1) return;
        
        Button button = this.window.getContainer().getNode("btn_" + slot);
        button.setIcon(new ItemStack(Material.AIR));
    }
    
    public void open(Player p, MarketChief chief) {
        RoninMarket.WINDOW.getContext(p).open(this.window, InventoryType.CHEST, "Marktleiter-" + chief.getName());
    }
}
