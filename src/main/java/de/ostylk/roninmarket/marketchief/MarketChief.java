package de.ostylk.roninmarket.marketchief;

import de.ostylk.roninmarket.market.Market;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import net.citizensnpcs.api.npc.NPCRegistry;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

public abstract class MarketChief {

    public static final int INVENTORY_SIZE = 54;

    private final String name;
    private final Location location;

    private int npcID;
    private NPC npc;

    private final MarketChiefLinkage linkedMarkets;
    private final MarketChiefWindow chiefWindow;
    
    public MarketChief(String name, Location location) {
        this.name = name;
        this.location = location;
        this.linkedMarkets = new MarketChiefLinkage();
        this.chiefWindow = new MarketChiefWindow(this.linkedMarkets);
        this.spawn();
    }

    public MarketChief(String name, int npcID, MarketChiefLinkage linkage) {
        this.name = name;
        this.npcID = npcID;
        this.npc = CitizensAPI.getNPCRegistry().getById(npcID);
        this.location = this.npc.getStoredLocation();
        this.linkedMarkets = linkage;
        this.chiefWindow = new MarketChiefWindow(this.linkedMarkets);
    }

    private void spawn() {
        NPCRegistry registry = CitizensAPI.getNPCRegistry();
        this.npc = registry.createNPC(EntityType.PLAYER, "Marktleiter-" + name);
        this.npcID = this.npc.getId();
        this.npc.spawn(this.location);
    }

    public void despawn() {
        this.npc.destroy();
    }

    public void update() {
        this.chiefWindow.update();
    }
    
    public void link(Market market, int slot) {
        this.linkedMarkets.link(slot, market);
        this.chiefWindow.link(market, slot);
    }

    public void unlink(Market market) {
        this.chiefWindow.unlink(this.linkedMarkets.unlink(market));
    }

    public void openInventory(Player p) {
        this.chiefWindow.open(p, this);
    }

    public String getName() { return this.name; }
    public Location getLocation() { return this.location; }
    public int getNpcID() { return this.npcID; }
    public NPC getNPC() { return this.npc; }
    public MarketChiefLinkage getLinkedMarkets() { return this.linkedMarkets; }
}
