package de.ostylk.roninmarket.marketchief;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.inventory.EquipmentSlot;

public class MarketChiefListener implements Listener {

    private final MarketChiefManager marketChiefManager;

    public MarketChiefListener(MarketChiefManager marketChiefManager) {
        this.marketChiefManager = marketChiefManager;
    }

    @EventHandler
    public void onRight(PlayerInteractAtEntityEvent e) {
        if (e.getHand() != EquipmentSlot.HAND) return;
        if (!e.getRightClicked().hasMetadata("NPC")) return;

        MarketChief chief = this.marketChiefManager.getMarketChief(e.getRightClicked());
        if (chief != null) {
            chief.openInventory(e.getPlayer());
        }
    }
}
