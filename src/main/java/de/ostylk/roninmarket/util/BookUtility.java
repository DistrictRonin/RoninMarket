package de.ostylk.roninmarket.util;

import net.minecraft.server.v1_14_R1.EntityPlayer;
import net.minecraft.server.v1_14_R1.EnumHand;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_14_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class BookUtility {

    public static void openBook(ItemStack is, Player p) {
        ItemStack held = p.getInventory().getItemInMainHand();
        p.getInventory().setItemInMainHand(is);
        sendPacket(is, p);
        p.getInventory().setItemInMainHand(held);
    }

    private static void sendPacket(ItemStack is, Player p) {
        EntityPlayer ep = ((CraftPlayer) p).getHandle();
        ep.openBook(CraftItemStack.asNMSCopy(is), EnumHand.MAIN_HAND);
    }
}
