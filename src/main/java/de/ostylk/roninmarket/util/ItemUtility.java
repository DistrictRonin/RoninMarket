package de.ostylk.roninmarket.util;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

public class ItemUtility {

    public static ItemStack rename(ItemStack is, String name) {
        ItemMeta im = is.hasItemMeta() ? is.getItemMeta() : Bukkit.getItemFactory().getItemMeta(is.getType());
        im.setDisplayName(name);
        is.setItemMeta(im);
        return is;
    }

    public static ItemStack lore(ItemStack is, List<String> lore) {
        ItemMeta im = is.hasItemMeta() ? is.getItemMeta() : Bukkit.getItemFactory().getItemMeta(is.getType());
        im.setLore(lore);
        is.setItemMeta(im);
        return is;
    }

    public static ItemStack itemFromNameAndData(String name, int data) {
        return new ItemStack(Material.getMaterial(name), 1, (short) data);
    }
}
