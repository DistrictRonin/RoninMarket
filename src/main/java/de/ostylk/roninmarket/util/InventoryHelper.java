package de.ostylk.roninmarket.util;

import org.bukkit.inventory.ItemStack;

public class InventoryHelper {

    public static int countItem(ItemStack[] contents, ItemStack check) {
        int amount = 0;
        for (ItemStack is : contents) {
            if (is != null && is.isSimilar(check)) {
                amount += is.getAmount();
            }
        }
        return amount;
    }

    public static void removeItem(ItemStack[] contents, ItemStack check, int amount) {
        for (int i = contents.length - 1; i >= 0; i--) {
            if (amount <= 0) break;

            ItemStack is = contents[i];
            if (is != null && is.isSimilar(check)) {
                int count = is.getAmount();
                if (count > amount) {
                    count -= amount;
                    amount = 0;
                } else if (count == amount) {
                    count = 0;
                    amount = 0;
                } else { // count < amount
                    amount -= count;
                    count = 0;
                }

                if (count == 0) {
                    contents[i] = null;
                } else {
                    is.setAmount(count);
                    contents[i] = is;
                }
            }
        }
    }
    
    public static int countFreeSpace(ItemStack[] contents, ItemStack check) {
        int amount = 0;
        for (ItemStack item : contents) {
            if (item == null) {
                amount += 64;
            } else {
                if (item.isSimilar(check)) {
                    amount += item.getMaxStackSize() - item.getAmount();
                }
            }
        }
        
        return amount;
    }
}
