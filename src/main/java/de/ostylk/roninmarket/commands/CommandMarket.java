package de.ostylk.roninmarket.commands;

import de.ostylk.baseapi.modules.command.Command;
import de.ostylk.baseapi.modules.command.Help;
import de.ostylk.roninmarket.RoninMarket;
import de.ostylk.roninmarket.market.Market;
import de.ostylk.roninmarket.market.transfer.TransferInfo;
import org.bukkit.entity.Player;

public class CommandMarket {

    @Help("Erstellt einen Markt")
    @Command("mt.create")
    public void marketCreate(Player sender) {
        Market market = RoninMarket.getInstance().getMarketManager().createMarket(sender);
        market.showParticles(sender);
        sender.sendMessage("\u00a7aMarkt \u00a7e#" + market.getID() + " \u00a7aerfolgreich erstellt!");
    }

    @Help("Löscht einen Markt")
    @Command("mt.remove")
    public void marketRemove(Player sender, Market market) {
        RoninMarket.getInstance().getMarketManager().deleteMarket(market.getID());
        sender.sendMessage("\u00a7aMarkt #\u00a7e" + market.getID() + " \u00a7aerfolgreich gelöscht.");
    }

    @Help("Zeigt einen Marktbereich mit Partikeln an")
    @Command("mt.show")
    public void marketShow(Player sender, Market market) {
        market.showParticles(sender);
    }

    @Help("Listet alle Märkte auf")
    @Command("mt.list")
    public void marketList(Player sender) {
        RoninMarket.getInstance().getMarketManager().getMarkets().values().forEach(market -> {
            sender.sendMessage("\u00a7e#" + market.getID() + ": \u00a77@" + market.getWorld().getName() + " <-> " +
                    market.getArea().encode());
        });
    }

    @Help("Interner Befehl")
    @Command("mt.accept")
    public void marketAccept(Player sender) {
        TransferInfo info = RoninMarket.getInstance().getMarketManager().getTransferManager().getInfo(sender);
        if (info == null) return;

        switch (info.getState()) {
            case WAIT_OWNER_CONFIRM:
                if (info.getMarket().getOwnerName().equals(sender.getName())) {
                    RoninMarket.getInstance().getMarketManager().getTransferManager().confirmOwner(info.getMarket());
                }
                break;
            case WAIT_TARGET_CONFIRM:
                if (info.getTarget().getName().equals(sender.getName())) {
                    RoninMarket.getInstance().getMarketManager().getTransferManager().confirmTarget(sender);
                }
                break;
        }
    }

    @Help("Interner Befehl")
    @Command("mt.decline")
    public void marketDecline(Player sender) {
        TransferInfo info = RoninMarket.getInstance().getMarketManager().getTransferManager().getInfo(sender);
        if (info == null) return;

        switch (info.getState()) {
            case WAIT_OWNER_CONFIRM:
                // Only allow the owner to cancel
                if (sender.getName().equals(info.getMarket().getOwnerName())) {
                    RoninMarket.getInstance().getMarketManager().getTransferManager().cancelTransfer(sender);
                }
                break;
            case WAIT_TARGET_CONFIRM:
                // Only allow the target to cancel
                if (sender.getName().equals(info.getTarget().getName())) {
                    RoninMarket.getInstance().getMarketManager().getTransferManager().cancelTransfer(sender);
                }
                break;
        }
    }
}
