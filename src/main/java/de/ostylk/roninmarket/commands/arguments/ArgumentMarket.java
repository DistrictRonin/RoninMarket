package de.ostylk.roninmarket.commands.arguments;

import de.ostylk.baseapi.modules.command.argument.ArgumentParser;
import de.ostylk.baseapi.modules.command.argument.ParsePayload;
import de.ostylk.roninmarket.RoninMarket;
import de.ostylk.roninmarket.market.Market;
import org.bukkit.command.CommandSender;

import java.util.Set;
import java.util.stream.Collectors;

public class ArgumentMarket implements ArgumentParser<Market> {

    @Override
    public Set<String> getAllPossibilities() {
        return RoninMarket.getInstance().getMarketManager().getMarkets().values().stream()
                .map(market -> String.valueOf(market.getID())).collect(Collectors.toSet());
    }

    @Override
    public ParsePayload<Market> parse(CommandSender sender, String arg) {
        try {
            int id = Integer.parseInt(arg);
            Market market = RoninMarket.getInstance().getMarketManager().getMarket(id);

            return market != null ?
                    ParsePayload.success(market) :
                    ParsePayload.error("Dieser Markt existiert nicht");
        } catch (NumberFormatException e) {
            return ParsePayload.error("Das ist keine gültige Markt-ID");
        }
    }

    @Override
    public Class<Market> getType() {
        return Market.class;
    }
}
