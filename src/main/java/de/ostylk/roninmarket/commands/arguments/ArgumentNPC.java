package de.ostylk.roninmarket.commands.arguments;

import de.ostylk.baseapi.modules.command.argument.ArgumentParser;
import de.ostylk.baseapi.modules.command.argument.ParsePayload;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import org.bukkit.command.CommandSender;

import java.util.HashSet;
import java.util.Set;

public class ArgumentNPC implements ArgumentParser<NPC> {

    @Override
    public Set<String> getAllPossibilities() {
        return new HashSet<>();
    }

    @Override
    public ParsePayload<NPC> parse(CommandSender sender, String arg) {
        if (arg.equals("$selected")) {
            NPC selectedNPC = CitizensAPI.getDefaultNPCSelector().getSelected(sender);
            if (selectedNPC == null) {
                return ParsePayload.error("Du musst einen NPC mit /npc select auswählen");
            } else {
                return ParsePayload.success(selectedNPC);
            }
        } else {
            try {
                int id = Integer.parseInt(arg);
                NPC npc = CitizensAPI.getNPCRegistry().getById(id);
                if (npc != null) {
                    return ParsePayload.success(npc);
                } else {
                    return ParsePayload.error("Dieser NPC existiert nicht");
                }
            } catch (NumberFormatException e) {
                return ParsePayload.error("Das ist keine gültige NPC ID");
            }
        }
    }

    @Override
    public Class<NPC> getType() {
        return NPC.class;
    }
}
