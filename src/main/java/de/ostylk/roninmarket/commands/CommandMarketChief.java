package de.ostylk.roninmarket.commands;

import de.ostylk.baseapi.modules.command.Command;
import de.ostylk.baseapi.modules.command.Help;
import de.ostylk.baseapi.modules.command.Optional;
import de.ostylk.roninmarket.RoninMarket;
import de.ostylk.roninmarket.market.Market;
import de.ostylk.roninmarket.marketchief.MarketChief;
import net.citizensnpcs.api.npc.NPC;
import org.bukkit.entity.Player;

public class CommandMarketChief {

    @Help("Spawnt einen Marktleiter")
    @Command("mt.ml.spawn")
    public void chiefSpawn(Player sender, String name) {
        RoninMarket.getInstance().getMarketChiefManager().spawnMarketChief(name, sender.getLocation());
        sender.sendMessage("\u00a77Marktleiter \u00a7aerfolgreich \u00a77erstellt.");
    }

    @Help("Entfernt einen Marktleiter")
    @Command("mt.ml.remove")
    public void chiefRemove(Player sender, @Optional("$selected") NPC npc) {
        RoninMarket.getInstance().getMarketChiefManager().removeMarketChief(npc.getEntity());
        sender.sendMessage("\u00a77Marktleiter \u00a7aerfolgreich \u00a77entfernt.");
    }

    @Help("Verlinkt einen Markt zum Marktleiter")
    @Command("mt.ml.link")
    public void chiefLink(Player sender, Market market, int slot, @Optional("$selected") NPC npc) {
        if (slot < 0) {
            sender.sendMessage("\u00a7cEs gibt keinen \"Negativen-Slot\".");
            return;
        } else if (slot > MarketChief.INVENTORY_SIZE - 2) {
            sender.sendMessage("\u00a7cDu darfst nur die Slots von 0-" + (MarketChief.INVENTORY_SIZE - 2) + " verwenden.");
            return;
        }

        MarketChief chief = RoninMarket.getInstance().getMarketChiefManager().getMarketChief(npc.getEntity());
        RoninMarket.getInstance().getMarketChiefManager().linkMarket(chief, market, slot);
        sender.sendMessage("\u00a77Markt \u00a7e" + market.getID() + " \u00a77erfolgreich verlinkt.");
    }

    @Help("Entfernt eine Verlinkung von einem Markt zum Marktleiter")
    @Command("mt.ml.unlink")
    public void chiefUnlink(Player sender, Market market, @Optional("$selected") NPC npc) {
        MarketChief chief = RoninMarket.getInstance().getMarketChiefManager().getMarketChief(npc.getEntity());
        RoninMarket.getInstance().getMarketChiefManager().unlinkMarket(chief, market);
        sender.sendMessage("\u00a77Alle Verlinkungen zum Markt \u00a7e" + market.getID() + " \u00a77wurden "
                + " von diesem Marktleiter entfernt.");
    }
}
